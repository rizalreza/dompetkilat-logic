package main

import (
	"fmt"
)

type result struct {
    name string
    count int
}

func main() {
	count := countElements([]string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"})
	fmt.Println(count	)
}

// Count duplicate elements in a slice
func countElements(slice []string) map[string]int {
	result := make(map[string]int)
	for _, value := range slice {
		result[value]++
	}
	return result
}
