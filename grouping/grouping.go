package main

import (
	"fmt"
)

func main() {
	group := group([]string{"a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"})
	fmt.Println(group)
}

func group(s []string) [][]string {
	m := make(map[string][]string)
	for _, v := range s {
		m[v] = append(m[v], v)
	}
	var result [][]string
	for _, v := range m {
		result = append(result, v)
	}
	return result
}