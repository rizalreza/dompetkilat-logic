package main

import (
	"fmt"
)

func main()  {
	primes := findPrimeFromRange(11, 40)
	fmt.Println(primes)	
}

func findPrimeFromRange(rangeStart int, rangeEnd int) []int {
	var primes []int
	for i:=rangeStart; i<=rangeEnd; i++ {
		if isPrime(i) {
			primes = append(primes, i)
		}
	}
	return primes
}

func isPrime(num int) bool {
	if num < 2 {
		return false
	}
	for i := 2; i < num; i++ {
		if num % i == 0 {
			return false
		}
	}
	return true
}